(function ($) {
    "use strict";

    //Preloader
    $(window).on('load', function (event) {
        $('.js-preloader').delay(500).fadeOut(500);
    });

    //Sticky Header
    var wind = $(window);
    var sticky = $('.header-wrap');
    wind.on('scroll', function () {
        var scroll = wind.scrollTop();
        if (scroll < 10) {
            sticky.removeClass('sticky');
        } else {
            sticky.addClass('sticky');
        }
    });

    //Close Sidebar
    $('.sidebar-btn').on('click', function(){
      $('.db-wrap').toggleClass('close-sidebar');
    });

    //Open User Option Menu
    $('.user-option').on('click', function() {
        $(this).children('ul').toggleClass('open');
        $('.body_overlay').addClass('open');
        if($('.notification-wrap').hasClass('open')){
            $('.notification-wrap').removeClass('open')
        }
    });
    $('.body_overlay').on('click', function() {
        $(this).removeClass('open');
        $('.user-option').children('ul').removeClass('open');
    });

    // Open Notification Menu
    $('.notification-btn').on('click', function() {
        $(this).children('.notification-wrap').toggleClass('open');
        $('.body_overlay').addClass('open');
        if($('.user-option').children('ul').hasClass('open')){
            $('.user-option').children('ul').removeClass('open')
        }
    });
    $('.body_overlay').on('click', function() {
        $(this).removeClass('open');
        $('.notification-wrap').removeClass('open');
    });

    //Language Dropdown
    $(".language-option").each(function () {
        var each = $(this)
        each.find(".lang-name").html(each.find(".language-dropdown-menu a:nth-child(1)").text());
        var allOptions = $(".language-dropdown-menu").children('a');
        each.find(".language-dropdown-menu").on("click", "a", function () {
            allOptions.removeClass('selected');
            $(this).addClass('selected');
            $(this).closest(".language-option").find(".lang-name").html($(this).text());
        });
    })

    //Open Action Dropdown
    $('.action-dropdown').on('click', function() {
        $(this).children('ul').toggleClass('open');
    });

    // Product Quantity
     var minVal = 1,
     maxVal = 20;
     $(".increaseQty").on('click', function () {
         var $parentElm = $(this).parents(".qtySelector");
         $(this).addClass("clicked");
         setTimeout(function () {
             $(".clicked").removeClass("clicked");
         }, 100);
         var value = $parentElm.find(".qtyValue").val();
         if (value < maxVal) {
             value++;
         }
         $parentElm.find(".qtyValue").val(value);
     });
     
     // Decrease product quantity on cart page
     $(".decreaseQty").on('click', function () {
         var $parentElm = $(this).parents(".qtySelector");
         $(this).addClass("clicked");
         setTimeout(function () {
             $(".clicked").removeClass("clicked");
         }, 100);
         var value = $parentElm.find(".qtyValue").val();
         if (value > 1) {
             value--;
         }
         $parentElm.find(".qtyValue").val(value);
     });

    //Category menu Dropdown
    $('.navbar-nav .nav-item .nav-link').on('click', function() {
        if ( $(this).parent('.nav-item').siblings().children('ul').hasClass('show')) {
            $(this).parent('.nav-item').siblings().children('ul').removeClass('show');
        }
    });
    // $('.sidebar-menu li a').on('click', function() {
    //     if ( $(this).parent().hasClass('has-sub')) {
    //       $(this).removeAttr('href');
    //     } else{
    //       $(this).addAttr('href');
    //     }
        
    //     var element = $(this).parent('li');
    //     if (element.hasClass('open')) {
    //         element.removeClass('open');
    //         element.find('li').removeClass('open');
    //         element.find('ul').slideUp();
    //     } else {
    //         element.addClass('open');
    //         element.children('ul').slideDown();
    //         element.siblings('li').children('ul').slideUp();
    //         element.siblings('li').removeClass('open');
    //         element.siblings('li').find('li').removeClass('open');
    //         element.siblings('li').find('ul').slideUp();
    //     }
    // });

    
    
    //Doughunt 
    // var myChart = new Chart("myChart", {
    //   type: 'doughnut',
    //     data: {
    //       labels: xValues,
    //       datasets: [{
    //       backgroundColor: "rgba(0,0,0,1.0)",
    //       borderColor: "rgba(0,0,0,0.1)",
    //       data: yValues
    //   }]
    //   },
    //   options: {
    //     responsive: true,
    //     plugins: {
    //       legend: {
    //         position: 'top',
    //       },
    //       title: {
    //         display: true,
    //         text: 'Chart.js Doughnut Chart'
    //       }
    //     }
    //   }
    // });
      
})(jQuery);


// function to set a given theme/color-scheme
function setTheme(themeName) {
  localStorage.setItem('pos_theme', themeName);
  document.documentElement.className = themeName;
}
// function to toggle between light and dark theme
function toggleTheme() {
  if (localStorage.getItem('pos_theme') === 'theme-dark') {
      setTheme('theme-light');
  } else {
      setTheme('theme-dark');
  }
}
// Immediately invoked function to set the theme on initial load
(function () {
  if (localStorage.getItem('pos_theme') === 'theme-dark') {
      setTheme('theme-dark');
      document.getElementById('slider').checked = false;
  } else {
      setTheme('theme-light');
      document.getElementById('slider').checked = true;
  }
})();